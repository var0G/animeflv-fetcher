import re
import json
import httpx
import asyncio

SUCCESS = 200
HEADERS = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5)\
            AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36",
}


def get_chapters_urls(url):
    q = httpx.get(url, headers=HEADERS)

    if q.status_code == SUCCESS:
        total = re.findall(r"var episodes = \[(.*?)\];", q.text)[0]
        total = len(total.split("]")) - 1

        ver_url = url.replace("/anime/", "/ver/")
        return [ver_url + f"-{i}" for i in range(1, total + 1)]
    return None


async def get_mirrors(urls):

    async with httpx.AsyncClient() as client:
        tasks = (client.get(url, headers=HEADERS) for url in urls)
        reqs = await asyncio.gather(*tasks)

    data = list()
    for req in reqs:
        if req.status_code == SUCCESS:
            meta_data = {"url": str(req.url), "mirrors": list()}
            mirrors = re.findall(r"var videos = {(.*?)};", req.text)[0]
            mirrors = json.loads("{" + mirrors + "}")
            for mirror in mirrors["SUB"]:
                try:
                    meta_data["mirrors"].append(mirror["url"])
                except KeyError:
                    pass
            data.append(meta_data)
    return data


if __name__ == "__main__":

    # urls = get_chapters_urls("https://www3.animeflv.net/anime/one-piece-tv")
    urls = get_chapters_urls("https://www3.animeflv.net/anime/ijiranaide-nagatorosan")
    print("Fetch done")
    fetch = {"anime": "Nagatoro", "urls": asyncio.run(get_mirrors(urls))}
    print("Fetch urls")
    with open("fetch_data.json", "w") as f:
        json.dump(fetch, f)
